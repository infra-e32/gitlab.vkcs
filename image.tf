data "vkcs_images_image" "image" {
  name        = "${var.image}"
  most_recent = true
}