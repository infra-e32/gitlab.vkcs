data "template_file" "template" {
  template = "${file("./templates/rpm.tpl")}"
  vars = {
    password = random_password.password.result
  }
}

data "template_cloudinit_config" "cloudinit" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.template.rendered
  }
}

resource "local_file" "cloudinit_file" { 
  #  content  = "${data.null_data_source.ansible_inventory_file.outputs.name}" 
   content = templatefile("./templates/rpm.tpl", 
   { 
     password = random_password.password.result 
   } 
   ) 
 filename = "./hosts/rpm.ini" 
}
